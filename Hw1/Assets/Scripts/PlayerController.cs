﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    private int count;
    private Rigidbody rb;
    public float speed;
    public Text countText;
    public Text winText;

    private void Start()
    {
        count = 0;
        setCountText();
        rb = GetComponent<Rigidbody>();
        winText.text = "";
    }

    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement*speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            other.gameObject.SetActive(false);
            count += 1;
            setCountText();
        }
    }

    void setCountText()
    {

        countText.text = "Count: " + count.ToString();
        if(count >= 10)
        {
            winText.text = " YOU WIN ";
        }

    }



}
