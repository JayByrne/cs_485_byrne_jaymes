﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuControlls : MonoBehaviour {
    public AudioSource clickSound;
    public AudioSource mainMenuSound;
    // Use this for initialization
    void Start () {
        mainMenuSound.Play();
	}
	
	// Update is called once per frame
	void Update () {

         

}
    public void startGame()
    {
        mainMenuSound.Stop();
        clickSound.Play();
        
        SceneManager.LoadScene("MiniGame");
    }
    public void quitGame()
    {
        Application.Quit();
    }

}
