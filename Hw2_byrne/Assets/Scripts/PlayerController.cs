﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    private int count;
    private Rigidbody rb;
    public float speed;
    public Text countText;
    public Text winText;
    public float jump;
    public GameObject groundcheck;
    public GameObject pickCoins;
    public LayerMask whatisground;
    public AudioSource coinSound;
    public AudioSource coinSoundTwo;
    public AudioSource secret;
    public AudioSource jumpSound;
	
    private bool grounded = true;
   
    

    private void Start()
    {
        count = 0;
        setCountText();
        
        rb = GetComponent<Rigidbody>();
		
        winText.text = "";
    }
    private void Update()
    {
      

    }
    private void FixedUpdate()
    {
        grounded = Physics.CheckSphere(groundcheck.transform.position, 0.7f, whatisground);
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        bool moveZ = Input.GetKeyDown(KeyCode.Space);

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        if(moveZ == true && grounded)
        {
            rb.AddForce(0.0f, jump, 0.0f);
            jumpSound.Play();
        }

      
        rb.AddForce(movement*speed);

        if( gameObject.transform.position.y < -20)
        {
            this.gameObject.SetActive(false);
            this.gameObject.transform.position = new Vector3(0.0f, 0.5f, 0.0f);
            this.gameObject.SetActive(true);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickup"))
        {
            coinSound.Play();


            other.gameObject.SetActive(false);
            
            count += 1;
            setCountText();
        }
        if (other.gameObject.CompareTag("Pickup_Two"))
        {
            coinSoundTwo.Play();


            other.gameObject.SetActive(false);

            count += 1;
            setCountText();
        }
    }

    void setCountText()
    {

        countText.text = "Count: " + count.ToString();
        if(count == 10)
        {
            secret.PlayDelayed(0.3f);

            pickCoins.SetActive(false);
        }
        if (count == 13)
        {
			
            this.gameObject.SetActive(false);

        }
        

    }



}
