﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour {
   private Transform player;               // Reference to the player's position.
    
   private  NavMeshAgent nav;               // Reference to the nav mesh agent.
    public GameObject bosscheck;
    public LayerMask whatisboss;
   

    void Awake()
    {
        // Set up the references.
        player = GameObject.FindGameObjectWithTag("Player").transform;
       
        nav = GetComponent<NavMeshAgent>();
    }
    // Update is called once per frame
    void Update ()
    {
        NavMeshHit hit;
        NavMesh.SamplePosition(player.position, out hit, 100.0f, 1);
            nav.SetDestination(hit.position);
        
        
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.SetActive(false);
        }
    }
}
