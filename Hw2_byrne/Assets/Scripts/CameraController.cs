﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject Player;
    public GameObject overH;
    public GameObject thirdP;
    private Vector3 offset;
    private int count;
    
	// Use this for initialization
	void Start () {
        offset = transform.position - Player.transform.position;
        thirdP.SetActive(false);
        count = 0;
	}
	
	// Update is called once per frame
	void Update () {
        
        transform.position = Player.transform.position + offset;
        if (Input.GetKeyDown(KeyCode.T) && count % 2 == 0)
        {
            count += 1;
            thirdP.SetActive(true);
        }
        else if (Input.GetKeyDown(KeyCode.T))
        {
            thirdP.SetActive(false);
            count += 1;
        }


    }
}
