﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShowPauseMenu : MonoBehaviour {
    public Transform uiMenu;
    public Transform uiBack;

    public Transform overUIMenu;
    public AudioSource backround;
    public GameObject player;

    // Use this for initialization
    void Start () {
        uiBack.gameObject.SetActive(false);
        uiMenu.gameObject.SetActive(false);
        overUIMenu.gameObject.SetActive(false);
        backround.Play();
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pauseGame();
        }
        if (!player.activeInHierarchy)
        {
            
            uiBack.gameObject.SetActive(true);
            overUIMenu.gameObject.SetActive(true);
        }
        if (overUIMenu.gameObject.activeInHierarchy)
        {
            backround.Stop();
        }

    }
    public void pauseGame()
    {
        if (uiMenu.gameObject.activeInHierarchy == false)
        {
            uiBack.gameObject.SetActive(true);
            uiMenu.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            uiBack.gameObject.SetActive(false);
            uiMenu.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
    }
    public void exitGame()
    {
        Application.Quit(); 
    }
    public void restart()
    {
        
        SceneManager.LoadScene("MiniGame");
        
    }
}
