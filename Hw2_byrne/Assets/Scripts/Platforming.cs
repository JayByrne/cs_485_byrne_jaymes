﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platforming : MonoBehaviour {
    
    public Transform eleOne;
    public int multiplier;
    private Vector3 topElevator;
    private Vector3 bottomElevator;
    private bool upstate = false;
    public GameObject pickUps;
   // private Vector3 bottomElevator;
	// Use this for initialization
	void Start ()
    {
        topElevator = new Vector3(this.gameObject.transform.position.x, 10.0f, this.gameObject.transform.position.z);
        bottomElevator = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, this.gameObject.transform.position.z);
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {

        moveEleState();
       
        
	}

    public void moveEleState()
    {

        if (!pickUps.activeInHierarchy)
        {

            if (upstate == false)
            {
                eleOne.transform.position = Vector3.Lerp(eleOne.transform.position, topElevator, multiplier * Time.deltaTime);
                if (eleOne.transform.position.y > 9.9f)
                {
                    upstate = true;
                }

            }
            else
            {
                eleOne.transform.position = Vector3.Lerp(eleOne.transform.position, bottomElevator, multiplier * Time.deltaTime);

                if (eleOne.transform.position.y < bottomElevator.y + 0.5f)
                {
                    upstate = false;
                }
            }
        }



    }
}
